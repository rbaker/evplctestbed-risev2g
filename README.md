Modified copy of RISE-V2G for an EV Charging testbed with Raspberry Pi 4s, Devolo GreenPHY boards and custom cable. 

See original code at [https://github.com/V2GClarity/RISE-V2G](https://github.com/V2GClarity/RISE-V2G) 
